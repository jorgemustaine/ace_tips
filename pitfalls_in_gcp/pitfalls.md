---
marp: true
theme: uncover
_class: invert
style: |
  section {
    background-color: lightyellow;
  }
footer: ![opacity:.5](../static/images/globant_ico.png)
---
## Pitfalls in the GCP exam
![bg](../static/images/pitfall610.jpeg)

### how to overcome them successfully
### by jorgescalona & cesar diaz

---

<style scoped>
{
  font-size: 18px
}
</style>
![bg left](../static/images/pitfall_02.jpeg)
1. [create your kryterion Webassesor user account.](https://kryterion.force.com/support/s/article/How-do-I-create-a-Webassessor-Account?language=en_US)
 
1. What happen with your machine.
   * SO (macos, gnu/linux, guindows)
   * firewall  off
   * antivirus off
   * [Install sentynel kryterion software.](https://kryterion.force.com/support/s/article/Installing-Sentinel-Secure?language=en_US)

1. How about you network.
   * wifi or wire.
   * test your bandwitch.
       * [web client.](https://www.speedtest.net/)
       * [cli client.](https://github.com/sivel/speedtest-cli)
1. [Test your Machine. Kryterion system check](https://www.kryteriononline.com/systemcheck/)
1. tidy up your room. (pick up your dirty clothes)
   * isolated room.
   * pan 360 degrees.
   * close your (gf,bf,wife,wives,husband) talks. 


---
![bg opacity:.25](../static/images/pitfall_04.jpeg)
<style scoped>
{
  font-size: 26px
}
</style>
**key stuffs to achive your GCP cert.**

1. [ACE certification exam guide.](https://cloud.google.com/certification/guides/cloud-engineer)
1. [Dan sullivan gcp ACE certification official guide.](https://books.google.com.uy/books/about/Official_Google_Cloud_Certified_Associat.html?id=g1kRvAEACAAJ&redir_esc=y)
1. [Dan Sullivan on likeln.](https://www.linkedin.com/in/dansullivanpdx/)
1. [Globant Udemy](https://globant.udemy.com/)
1. [Google certified associate cloud engineer on udemy by dan sullivan.](https://globant.udemy.com/course/google-certified-associate-cloud-engineer-2019-prep-course)
![bg width:200px height:5cm right](../static/images/GCP-ACE_badge.png)
1. [gcloud cli tool](https://cloud.google.com/sdk/gcloud)
1. And of course all of catalog of qwiklabs offers by google challenge program.
---
# Mocking Test Demo 

[![Mocking Test](../static/images/dan_sullivan_ace_udemy_course.png)](https://drive.google.com/file/d/1EA_OT8-DGi8QSHLHaccneu01l-1L26e1/preview "Mocking Exam Link")

---
![](../static/images/game_over.webp)
# Questions ?

---

![bg opacity:.25](../static/images/little_padawans.jpeg)

### Next talks
[WIP]
* Use python on GCP integrations (with the padawans Cecilia Giudice cecilia.giudice@globant.com and Juan Pablo Gomez juan.gu@globant.com )
* [Make mentoring with git for globant.](https://gitlab.com/g6623/mentorship-meetings)

